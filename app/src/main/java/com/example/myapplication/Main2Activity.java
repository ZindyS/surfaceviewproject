package com.example.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Main2Activity extends AppCompatActivity {

    //Переменная
    RecyclerView rec;

    //Создание ShPr
    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Айдишки
        rec = findViewById(R.id.rec);

        //Настройка ShPr и RecView
        sh = getSharedPreferences("Bruh", 0);
        RecAdapter adapter = new RecAdapter(this, sh.getInt("stat", 0));
        rec.setAdapter(adapter);

        //Настраиваем RecView
        rec.setLayoutManager(new GridLayoutManager(this, 3));
    }

    //Считываем клики
    public void onClicked(View view) {
        Intent intent = new Intent(Main2Activity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}