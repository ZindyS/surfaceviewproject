package com.example.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import javax.security.auth.callback.Callback;

public class MySurface extends SurfaceView implements SurfaceHolder.Callback {

    //Многа переменных

    private SurfaceHolder surfaceHolder = null;

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int screenWidth = 0;

    private int screenHeight = 0;

    Path path;

    public MySurface(Context context) {
        super(context);
        setFocusable(true);

        //Настраиваем Holder
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);

        //Настройка полотна
        paint.setStyle(Paint.Style.STROKE);
        int[] b = {255, 255, 255, 255};
        setSettings(3, b);
        setZOrderOnTop(true);

        //setBackgroundColor(Color.RED);
    }

    //Я ХЗ что это делает, но оно тут есть
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        screenHeight = getHeight();
        screenWidth = getWidth();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    //Когда на SurfaceView нажали
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            path = new Path();
            path.moveTo(event.getX(), event.getY());
            path.addCircle(event.getX(), event.getY(), 1, Path.Direction.CW);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            path.lineTo(event.getX(), event.getY());
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            path.lineTo(event.getX(), event.getY());
        }

        if (path != null) {
            Canvas canvas = surfaceHolder.lockCanvas();
            canvas.drawPath(path, paint);
            surfaceHolder.unlockCanvasAndPost(canvas);
        }

        return true;
    }

    //Настройка "кисти"
    public void setSettings(int size, int[] color) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(size);
        paint.setARGB(color[0], color[1], color[2], color[3]);
    }

    //Переключение на заливку
    public void setStyle() {
        paint.setStyle(Paint.Style.FILL);
    }
}
