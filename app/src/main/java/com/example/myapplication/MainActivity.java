package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;

public class MainActivity extends Activity implements SeekBar.OnSeekBarChangeListener {

    //Многа переменных

    RecyclerView rec;

    SharedPreferences sh;

    SharedPreferences.Editor ed;

    static int razmer_kar = 3;
    static int rasmer_last = 3;
    static int last = 0;

    static boolean er = false;
    static boolean sizee = false;
    static boolean isSizee = true;
    boolean rece = false;

    TextView color;

    static LinearLayout colorid;
    LinearLayout typee;
    static LinearLayout somecolor;
    static LinearLayout size;
    static LinearLayout somesize;
    static SeekBar seekBar;

    static int[] colorir= {255, 255, 255, 255};

    static int[] colorlast= {255, 0, 0, 0};

    EditText alpha;
    EditText red;
    EditText blue;
    EditText green;
    static EditText sizuha;

    static MySurface surfaceViewThread;

    LinearLayout drawTextCanvas;

    //Настройка панели инстументов (работает хренова)
    public static void SomeFunc(int position) {
        if (last == 0) {
            last = 1;
            colorid.setVisibility(View.VISIBLE);
            sizee = false;
            somesize.setVisibility(View.INVISIBLE);
            sizuha.setText(String.valueOf(razmer_kar));
            seekBar.setProgress(razmer_kar/3);
            size.setVisibility(View.VISIBLE);
            isSizee = true;
            surfaceViewThread.setSettings(razmer_kar, colorir);
        } else if (last == 1) {
            last = 2;
            colorid.setVisibility(View.INVISIBLE);
            sizee = false;
            somesize.setVisibility(View.INVISIBLE);
            somecolor.setVisibility(View.INVISIBLE);
            er = false;
            sizuha.setText(String.valueOf(rasmer_last));
            seekBar.setProgress(rasmer_last/3);
            size.setVisibility(View.VISIBLE);
            isSizee = true;
            surfaceViewThread.setSettings(rasmer_last, colorlast);
        } else {
            last = 0;
            colorid.setVisibility(View.VISIBLE);
            sizee = false;
            somesize.setVisibility(View.INVISIBLE);
            somecolor.setVisibility(View.INVISIBLE);
            er = false;
            size.setVisibility(View.INVISIBLE);
            isSizee = false;
            surfaceViewThread.setSettings(razmer_kar, colorir);
            surfaceViewThread.setStyle();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Создаём поверхность для рисования
        surfaceViewThread = new MySurface(getApplicationContext());

        //Многа айдишек
        color = findViewById(R.id.color);
        size = findViewById(R.id.size);
        colorid = findViewById(R.id.colorid);
        typee = findViewById(R.id.type);
        somecolor = findViewById(R.id.somecolor);
        alpha = findViewById(R.id.alpha);
        red = findViewById(R.id.red);
        blue = findViewById(R.id.green);
        green = findViewById(R.id.blue);
        somesize = findViewById(R.id.nicesize);
        sizuha = findViewById(R.id.sizeee);
        seekBar = findViewById(R.id.seekBar);
        rec = findViewById(R.id.recyclerView);

        //Настойка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();

        //Считываем измения состаяния seekBar'а
        seekBar.setOnSeekBarChangeListener(this);

        //Настройка выпадающих менюшек
        somesize.setVisibility(View.INVISIBLE);
        somecolor.setVisibility(View.INVISIBLE);

        //Считываем нажатия на кнопку карандаша/ластика
        typee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!rece) {
                    RecyclerAdapter adapter = new RecyclerAdapter();
                    somesize.setVisibility(View.INVISIBLE);
                    sizee = false;
                    somecolor.setVisibility(View.INVISIBLE);
                    er = false;
                    rec.setAdapter(adapter);
                    rec.setVisibility(View.VISIBLE);
                    rece = true;
                } else {
                    rec.setVisibility(View.INVISIBLE);
                    rece = false;
                }
//                if (last == 0) {
//                    last = 1;
//                    colorid.setVisibility(View.VISIBLE);
//                    sizee = false;
//                    somesize.setVisibility(View.INVISIBLE);
//                    sizuha.setText(String.valueOf(razmer_kar));
//                    seekBar.setProgress(razmer_kar/3);
//                    surfaceViewThread.setSettings(razmer_kar, colorir);
//                } else if (last == 1) {
//                    last = 2;
//                    colorid.setVisibility(View.INVISIBLE);
//                    sizee = false;
//                    somesize.setVisibility(View.INVISIBLE);
//                    somecolor.setVisibility(View.INVISIBLE);
//                    er = false;
//                    sizuha.setText(String.valueOf(rasmer_last));
//                    seekBar.setProgress(rasmer_last/3);
//                    surfaceViewThread.setSettings(rasmer_last, colorlast);
//                }
            }
        });

        //Считываем нажатие на кнопку цвета
        colorid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (er) {
                    er = false;
                    sizee = false;
                    rece = false;
                    somesize.setVisibility(View.INVISIBLE);
                    somecolor.setVisibility(View.INVISIBLE);
                    rec.setVisibility(View.INVISIBLE);
                } else {
                    er = true;
                    sizee = false;
                    rece = false;
                    somesize.setVisibility(View.INVISIBLE);
                    somecolor.setVisibility(View.VISIBLE);
                    rec.setVisibility(View.INVISIBLE);
                }
            }
        });

        //При нажатии Enter'а в EditText'е
        alpha.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (!TextUtils.isEmpty(alpha.getText())) {
                        colorir[0] = Integer.valueOf(String.valueOf(alpha.getText()));
                    }
                    surfaceViewThread.setSettings(razmer_kar, colorir);
                    return true;
                } else {
                    return false;
                }
            }
        });

        //При нажатии Enter'а в EditText'е
        red.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (!TextUtils.isEmpty(red.getText())) {
                        colorir[1] = Integer.valueOf(String.valueOf(red.getText()));
                    }
                    surfaceViewThread.setSettings(razmer_kar, colorir);
                    return true;
                } else {
                    return false;
                }
            }
        });

        //При нажатии Enter'а в EditText'е
        blue.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (!TextUtils.isEmpty(blue.getText())) {
                        colorir[2] = Integer.valueOf(String.valueOf(blue.getText()));
                    }
                    surfaceViewThread.setSettings(razmer_kar, colorir);
                    return true;
                } else {
                    return false;
                }
            }
        });

        //При нажатии Enter'а в EditText'е
        green.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (!TextUtils.isEmpty(green.getText())) {
                        colorir[3] = Integer.valueOf(String.valueOf(green.getText()));
                    }
                    surfaceViewThread.setSettings(razmer_kar, colorir);
                    return true;
                } else {
                    return false;
                }
            }
        });

        //Обработка нажатий на кнопку размер
        size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sizee) {
                    sizee = false;
                    rece = false;
                    rec.setVisibility(View.INVISIBLE);
                    somesize.setVisibility(View.INVISIBLE);
                } else {
                    sizee = true;
                    er = false;
                    rece = false;
                    rec.setVisibility(View.INVISIBLE);
                    somecolor.setVisibility(View.INVISIBLE);
                    somesize.setVisibility(View.VISIBLE);
                }
            }
        });

        //При нажатии Enter'а в EditText'е
        somesize.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (!TextUtils.isEmpty(sizuha.getText())) {
                        if (last == 1) {
                            rasmer_last = Integer.valueOf(String.valueOf(sizuha.getText()));
                            surfaceViewThread.setSettings(rasmer_last, colorlast);
                        } else if (last == 0){
                            razmer_kar = Integer.valueOf(String.valueOf(sizuha.getText()));
                            surfaceViewThread.setSettings(razmer_kar, colorir);
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });

        //Настраиваем поверхность для рисования на экране
        drawTextCanvas = (LinearLayout)findViewById(R.id.bab);
        drawTextCanvas.addView(surfaceViewThread);
    }

    //При изменении значения SeekBar'a
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        sizuha.setText(String.valueOf(seekBar.getProgress()*3));
        if (last == 1) {
            rasmer_last = seekBar.getProgress()*3;
            surfaceViewThread.setSettings(rasmer_last, colorlast);
        } else if (last == 0){
            razmer_kar = seekBar.getProgress()*3;
            surfaceViewThread.setSettings(razmer_kar, colorir);
        }
    }

    //При нажатия на ползунок SeekBar'a
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    //При отпускании ползунка SeekBar'a
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    //При нажатии на "сохранить"
    public void onSaveClicked(View v) {

        //Сохраняем рисунок
        drawTextCanvas.setDrawingCacheEnabled(true);
        Bitmap bitmap = drawTextCanvas.getDrawingCache();
        try {
            int state = sh.getInt("stat", 0);
            FileOutputStream out = openFileOutput("UniqueFileName" + String.valueOf(state) + ".jpg", MODE_PRIVATE);
            ed.putInt("stat", (state + 1));
            ed.commit();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.close();
        } catch (Exception ignored) {
        }
        bitmap.recycle();
//        drawTextCanvas.setDrawingCacheEnabled(true);
//        Bitmap b = drawTextCanvas.getDrawingCache();
//        ContextWrapper cw = new ContextWrapper(getApplicationContext());
//        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//
//        //Сохранение картинки с помощью уникального номера (запотентовано ©)
//        int state = sh.getInt("stat", 0);
//        File file = new File(directory, "UniqueFileName" + String.valueOf(state) + ".jpg");
//        ed.putInt("stat", (state + 1));
//        ed.commit();
//
//        //Всё ещё сохранение рисунка
//        if (!file.exists()) {
//            Log.d("path", file.toString());
//            FileOutputStream fos = null;
//            try {
//                fos = new FileOutputStream(file);
//                b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//                fos.flush();
//                fos.close();
//            } catch (java.io.IOException e) {
//                e.printStackTrace();
//            }
//        }

        //Переход на след. активность
        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(intent);
        finish();
    }
}
