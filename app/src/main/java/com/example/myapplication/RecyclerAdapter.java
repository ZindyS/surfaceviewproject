package com.example.myapplication;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.VH> {

    String[] list = {"Карандаш", "Ластик", "Заливка"};

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.tool_item, parent, false));
    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, final int position) {
        holder.textView.setText(list[position]);
    }

    public class VH extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textView;
        public VH(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView6);
            textView.setOnClickListener(this);
        }

        //Тут я вроде пытался считывать клики на отдельные элементы RecView
        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                MainActivity.SomeFunc(position);
            }
        }
    }
}
