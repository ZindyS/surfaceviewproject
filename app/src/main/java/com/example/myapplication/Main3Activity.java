package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Main3Activity extends AppCompatActivity {

    EditText a, r, g, b;
    MySurfaceecaf come;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        a = findViewById(R.id.editText);
        r = findViewById(R.id.editText2);
        g = findViewById(R.id.editText3);
        b = findViewById(R.id.editText4);
        come = findViewById(R.id.view);
    }

    public void onSomeCl(View v) {
        come.setSett(Integer.valueOf(String.valueOf(a.getText())), Integer.valueOf(String.valueOf(r.getText())), Integer.valueOf(String.valueOf(g.getText())), Integer.valueOf(String.valueOf(b.getText())));
    }
}
