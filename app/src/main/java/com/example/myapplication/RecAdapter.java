package com.example.myapplication;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;

public class RecAdapter extends RecyclerView.Adapter<RecAdapter.VH> {

    private Context context;

    private int state;

    public RecAdapter (Context context, int stat) {
        this.context = context;
        this.state = stat;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(context).inflate(R.layout.item, parent, false));
    }

    @Override
    public int getItemCount() {
        return state;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        //Загрузка сохранённых рисунков
        ContextWrapper cw = new ContextWrapper(context);
        File file = context.getFileStreamPath("UniqueFileName" + position + ".jpg");
        holder.imageView1.setImageDrawable(Drawable.createFromPath(String.valueOf(file)));
    }

    public class VH extends RecyclerView.ViewHolder{
        ImageView imageView1, imageView2, imageView3;
        public VH(@NonNull View itemView) {
            super(itemView);
            imageView1 = itemView.findViewById(R.id.imageView5);
        }
    }
}
